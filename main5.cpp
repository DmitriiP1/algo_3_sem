/*
 * Даны n точек в пространстве. Никакие 4 точки не лежат в одной полуплоскости.
 * Найдите выпуклую оболочку этих точек за O(n2).
 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <queue>
#include <vector>


using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::queue;

struct Point {
    Point(): x(0), y(0), z(0), pos(0) {};
    Point(const long double& _x, const long double& _y, const long double& _z): x(_x), y(_y), z(_z), pos(0) {};

    long double x;
    long double y;
    long double z;

    long double pos;

    Point operator* (long double i) const;
    Point operator/ (long double i) const;
    Point operator+ (const Point& other) const;
    Point operator- (const Point& other) const;
    bool operator== (const Point& p) const;
};

bool comp1 (Point i, Point j) {
    return i.pos < j.pos;
}

bool Point::operator== (const Point& p) const {
    return this->x == p.x && this->y == p.y && this->z == p.z;
}

Point Point::operator* (long double i) const {
    return Point(this->x * i, this->y * i, this->z * y);
}

Point Point::operator/ (long double i) const {
    return Point(this->x / i, this->y / i, this->z / y);
}

Point Point::operator+ (const Point& other) const {
    return Point(this->x + other.x, this->y + other.y, this->z + other.z);
}

Point Point::operator- (const Point& other) const {
    return Point(this->x - other.x, this->y - other.y, this->z - other.z);
}

struct Vector {
    Vector() {
        P1 = {0, 0, 0};
        P2 = {0, 0, 0};
        a = {0, 0, 0};
    }
    Vector(const Point& P_1, const Point& P_2): P1(P_1), P2(P_2) {
        a.x = P_2.x - P_1.x;
        a.y = P_2.y - P_1.y;
        a.z = P_2.z - P_1.z;
    };

    Point P1, P2;
    Point a;

    Vector operator* (long double i) const;
    Vector operator/ (long double i) const;
    Vector operator+ (const Vector& other) const;
    Vector operator- (const Vector& other) const;

    long double Norm() {
        return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
    }
};

Vector Vector::operator* (long double i) const {
    return Vector(this->P1 * i, this->P2 * i);
}

Vector Vector::operator/ (long double i) const {
    if (i == 0) {
        return Vector();
    }
    return Vector(this->P1 / i, this->P2 / i);
}

Vector Vector::operator+ (const Vector& other) const {
    return Vector(this->P1 + other.P1, this->P2 + other.P2);
}

Vector Vector::operator- (const Vector& other) const {
    return Vector(this->P1 - other.P1, this->P2 - other.P2);
}

long double scalar_dot(const Vector& v1, const Vector& v2) {
    return v1.a.x * v2.a.x + v1.a.y * v2.a.y + v1.a.z * v2.a.z;
}

//Векторное произведение.
Vector cross_product(Vector v1, Vector v2) {
    Vector result;
    result.a.x = v1.a.y * v2.a.z - v2.a.y * v1.a.z;
    result.a.y = v2.a.x * v1.a.z - v1.a.x * v2.a.z;
    result.a.z = v1.a.x * v2.a.y - v2.a.x * v1.a.y;
    return result;
};

struct edge {
    edge(Point P_1, Point P_2): P1(P_1), P2(P_2) {};
    Point P1;
    Point P2;
    bool operator== (const edge& e) const;
};

struct triangle {
    triangle() {};
    triangle(Point P_1, Point P_2, Point P_3): P1(P_1), P2(P_2), P3(P_3) {};
    Point P1;
    Point P2;
    Point P3;

    bool operator== (const triangle& tr) const;
};

bool comp2 (triangle i, triangle j) {
    if (i.P1.pos < j.P1.pos) {
        return true;
    }
    else{
        if (i.P1.pos == j.P1.pos) {
            if (i.P2.pos < j.P2.pos) {
                return true;
            }
            else{
                if (i.P2.pos == j.P2.pos) {
                    if (i.P3.pos < j.P3.pos) {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool triangle::operator== (const triangle& tr) const {
    if ((this->P1 == tr.P1 && this->P2 == tr.P2 && this->P3 == tr.P3) ||
        (this->P1 == tr.P2 && this->P2 == tr.P3 && this->P3 == tr.P1) ||
        (this->P1 == tr.P3 && this->P2 == tr.P1 && this->P3 == tr.P2) ||
        (this->P1 == tr.P1 && this->P2 == tr.P3 && this->P3 == tr.P2) ||
        (this->P2 == tr.P2 && this->P1 == tr.P3 && this->P3 == tr.P1) ||
        (this->P3 == tr.P3 && this->P1 == tr.P2 && this->P2 == tr.P1)) {
        return true;
    }
    else{
        return false;
    }
}

bool edge::operator== (const edge& e) const{
    if ((this->P1 == e.P1 && this->P2 == e.P2) || (this->P1 == e.P2 && this->P2 == e.P1)) {
        return true;
    }
    else{
        return false;
    }
}

//Первая точка первого треугольника. Ищется как самая нижняя, с минимальной координатой x и y.
Point point1(vector<Point>& vertices) {
    size_t min_z = 0;
    for (size_t i = 1; i < vertices.size(); ++i) {
        if (vertices[i].z < vertices[min_z].z) {
            min_z = i;
        }
        else if (vertices[i].z == vertices[min_z].z) {
            if (vertices[i].x < vertices[min_z].x) {
                min_z = i;
            }
            else if (vertices[i].x == vertices[min_z].x) {
                if (vertices[i].y < vertices[min_z].y) {
                    min_z = i;
                }
            }
        }
    }
    return vertices[min_z];
}

//Находит полярный угол точки.
long double polarAngle(Point &P1, Point &P) {
    return scalar_dot(Vector({0, 0, 0}, {0, 0, 1}), Vector(P1, P))/(Vector(P1, P).Norm());
}
//Вторая точка первого треугольника.
//Ищем как точка, для которой угол между вектором, соединяющим первую точку и ее максимален.
Point point2(vector<Point>& vertices, Point P1) {
    long double min_angle = 1;
    Point P;
    for (Point v: vertices) {
        if (polarAngle(P1, v) < min_angle &&
            (P1.x != v.x || P1.y != v.y || P1.z != v.z)) {//Проверяем что точки не совпадают
            min_angle = polarAngle(P1, v);
            P = v;
        }
    }
    return P;
}
//Третья точка первого треугольника.
//Ищем как точка, для которой нормаль плоскости (P1, P2, P3) образует наименьший угол с вертикалью.
Point point3(vector<Point>& vertices, Point P1, Point P2) {
    Point P3;
    long double max_cos = 0;
    Vector n({0, 0, 0}, {0, 0, 1});//Вертикаль
    for (Point v: vertices) {
        Vector normal = cross_product(Vector(v, P1), Vector(v, P2));//тут я проверял векторные произведения как надо с точки зрения правой тройки, но не уверен. тут наверное это не важно тк модуль
        long double cos_n_n = scalar_dot(normal, n)/ normal.Norm();
        if (fabs(cos_n_n) >= max_cos &&
            (P1.x != v.x || P1.y != v.y || P1.z != v.z) &&
            (P2.x != v.x || P2.y != v.y || P2.z != v.z)) {//проверяем что точки не совпадают
            max_cos = fabs(cos_n_n);
            P3 = v;
        }
    }
    return P3;
}

//Угол между нормалями плоскостей (P1, P2, P3) и (P2, P3, P).
long double angle(Point P1, Point P2, Point P3, Point P) {
    Vector normal1 = cross_product(Vector(P1, P2), Vector(P1, P3));//тут я проверял векторные произведения как надо с точки зрения правой тройки, но не уверен. тут это должно быть важно
    Vector normal2 = cross_product(Vector(P, P3), Vector(P, P2));
    long double cos_n_n = scalar_dot(normal1, normal2)/(normal1.Norm() * normal2.Norm());
    return cos_n_n;
}

//i-й шаг заворачивания.
//Находим точку P для которой угол между нормалями плоскостей (P1, P2, P3) и (P2, P3, P) минимален.
Point find_next_point(triangle current_triangle, vector<Point>& vertices) {
    Point answerPoint;
    long double cos_max = -1;
    for (Point P: vertices) {
        long double cos_n_n = angle(current_triangle.P1, current_triangle.P2, current_triangle.P3, P);
        if (cos_n_n >= cos_max) {
            cos_max = cos_n_n;
            answerPoint = P;
        }
    }
    return answerPoint;
}

//Проверяет вращали ли вокруг ребра е раньше.
bool find(vector<edge>& edges, edge e) {
    for (edge curr_edge: edges) {
        if (curr_edge == e) {   //Оператор сравнения ребер выше.
            return true;
        }
    }
    return false;
}

//Проверяет записан ли треугольник checked в ответ.
bool triangle_uses(vector<triangle>& triangles, triangle checked) {
    for (triangle tr: triangles) {
        if (checked == tr) {    //Оператор сравнения треугольников выше.
            return true;
        }
    }
    return false;
}

//Построение выпуклой 3D оболочки методом заворачивания подарка.
vector<triangle> convexHull(vector<Point> &vertices) {
    vector<triangle> answer;
    Point P1, P2, P3;

    //Первый треугольник.
    P1 = point1(vertices);          //Первая вершина первого треугольника.
    P2 = point2(vertices, P1);      //Вторая вершина первого треугольника.
    P3 = point3(vertices, P1, P2);  //Третья вершина первого треугольника.

    queue<triangle> triangles;      //Очередь из недавно найденных новых треугольников.
    //Треугольник задается тремя точками P1, P2, P3, вокург ребра (Р2, Р3) будем вращать.

    vector<edge> uses;              //Ребра вокург которых уже вращали.
    //По определенному выше опрератору сравнения ребер (P1, P2) = (P2, P1).

    triangles.push(triangle(P1, P2, P3));   //Будем вращать вокруг ребра (P2, P3).
    uses.emplace_back(edge(P2, P3));
    triangles.push(triangle(P2, P1, P3));   //Будем вращать вокруг ребра (P1, P3).
    uses.emplace_back(edge(P1, P3));
    triangles.push(triangle(P3, P1, P2));   //Будем вращать вокруг ребра (P1, P2).
    uses.emplace_back(edge(P1, P2));

    answer.emplace_back(triangle(P1, P2, P3));  //В ответ добавим первый треугольник.

    while (!triangles.empty()) {//Пока в очереди из новых найденных треугольников есть те, вокруг ребер которых пока не вращали.
        triangle current_triangle = triangles.front();
        triangles.pop();

        //i-й шаг алгоритма, ищем подходящую точку.
        Point P = find_next_point(current_triangle, vertices);

        //i-й шаг добавил вершину P  в множество вершни выпуклой оболочки и
        // два потенцильных для вращения ребра: (P, P2) и (P, P3).

        //Если вокург ребра (P2, P) еще вращали.
        if (!find(uses, edge(current_triangle.P2, P))) {
            triangles.push(triangle(current_triangle.P3, current_triangle.P2, P));
            uses.emplace_back(edge(current_triangle.P2, P));
        }
        //Если вокург ребра (P3, P) еще вращали.
        if (!find(uses, edge(current_triangle.P3, P))) {
            triangles.push(triangle(current_triangle.P2, current_triangle.P3, P));
            uses.emplace_back(edge(current_triangle.P3, P));
        }
        //Если треугольник (P2, P3, P) еще не в выпуклой оболочке.
        if (!triangle_uses(answer, triangle(current_triangle.P2, current_triangle.P3, P))) {
            answer.emplace_back(triangle(current_triangle.P2, current_triangle.P3, P));
        }
    }
    return answer;
}

//Центр масс -- внутренняя точка выпуклой оболочки.
Point CM(vector<Point>& vertices) {
    long double x = 0;
    long double y = 0;
    long double z = 0;
    for (Point v: vertices) {
        x += v.x;
        y += v.y;
        z += v.z;
    }
    x = x/vertices.size();
    y = y/vertices.size();
    z = z/vertices.size();
    return Point(x, y, z);
}

//Порядок обхода: по часовой/против.
void Print(Point cm, triangle& tr) {
    vector<Point> v;
    v.push_back(tr.P1);
    v.push_back(tr.P2);
    v.push_back(tr.P3);
    std::sort(v.begin(), v.end(), comp1);
    tr.P1 = v[0];
    tr.P2 = v[1];
    tr.P3 = v[2];

    Vector n = cross_product(Vector(tr.P1, tr.P2), Vector(tr.P1, tr.P3));//Нормаль плоскости треугольника.
    //Если нормаль направлена в то же полупространоство что и вектор, соединяющий центр масс и вершину треугольника.
    if (scalar_dot(Vector(cm, tr.P1), n) <= 0) {    //Если нет то меняем порядок вершин в треугольнике.
        Point tmp = tr.P2;
        tr.P2 = tr.P3;
        tr.P3 = tmp;
    }
}

int main(int argc, const char * argv[]) {
    size_t n, m;

    cin >> m;
    for (size_t j = 0; j < m; ++j) {
        cin >> n;

        vector<Point> vertices(n);
        for (size_t i = 0; i < n; ++i) {
            cin >> vertices[i].x >> vertices[i].y >> vertices[i].z;
            vertices[i].pos = i;
        }

        vector<triangle> triangles = convexHull(vertices);  //Треугольники которые в выпуклой оболочке.
        Point cm = CM(vertices);                    //Точка внутри выпуклой оболочки.

        for (size_t i = 0; i < triangles.size(); ++i) {
            Print(cm, triangles[i]);                //Чтобы получился ответ в виде который требуют.
        }

        sort(triangles.begin(), triangles.end(), comp2);    //Сортировка в лексикографическом порядке.

        cout << triangles.size() << endl;
        for (triangle tr: triangles) {
            cout << "3 " << tr.P1.pos << " " << tr.P2.pos << " " << tr.P3.pos << endl;
        }
    }
    return 0;
}
