/*
 * С помощбю z-функции найти все вхождения шаблона в строку.
 * Длина шаблона – p, длина строки – n. Время O(n + p), доп. память – O(p).
 */

#include <array>
#include <iostream>
#include <string>
#include <vector>

using std::string;

/*
 * Сначала найдем Z-функцию для шаблона. Затем итерируясь по символам строки будем вычислять значение Z-функции для
 * символа, если это значение равно длине шаблона, то вхождение найдено. Храним только вектор со значениями Z-функции
 * шаблона, поэтому доп. памаять О(р), реализация вычисления Z-функции О(n + p).
 */
class CSubstringFinder {
public:
    explicit CSubstringFinder(string& pattern): m_sPattern(pattern), m_len(pattern.length()) {
        //Вектор для хранения Z-функции шаблона
        m_zFunctionPattern.assign(m_len, 0);
        
        //Границы Z-блока
        m_left = 0;
        m_right = 0;
        
        //Z-функция для шаблона
        for (std::size_t i = 1; i < m_len; ++i) {
            m_zFunctionPattern[i] = std::max(0, std::min(static_cast<int>(m_right - i),
                                                 static_cast<int>(m_zFunctionPattern[i - m_left])));
            
            while (i + m_zFunctionPattern[i] < m_len
                   && pattern[m_zFunctionPattern[i]] == pattern[i + m_zFunctionPattern[i]])
            {
                ++m_zFunctionPattern[i];
            }
            
            //Обновляем границы Z-блока
            if (i + m_zFunctionPattern[i] > m_right) {
                m_left = i;
                m_right = i + m_zFunctionPattern[i];
            }
        }
        m_left = 0;
        m_right = 0;
    }
    
//--------------------------------------------------------------------------------------------------------------------//
    /*
     * Поиск подстрок в строке
     * str - строка, в которой ищем вхождение шаблона
     */
    void findSubstrings(string& str) {
        //Номер итерации в которой нашли позицию на которой начинается шаблон
        size_t it = 0;
        for (size_t i = 0; i < str.length(); ++i) {
            //Вычислим значение z-функции для символа, стоящего на i-й позиции
            size_t z = zFunction(str, i);
            
            /*Если значение Z-функции для i-го символа равно длине шаблона,
                то начиная с этой позиции начинаеся вхождение шаблона в строку*/
            if (z == m_len) {
                m_Buffer[it++] = i;//Про буфер смотри в private класса
            }
            //Если буфер заполнен то выводим все значения и начинаем заполнять его заново
            if (it == m_kBufferSize) {
                for (int j = 0; j < m_kBufferSize; ++j) {
                    std::cout << m_Buffer[j] << '\n';
                }
                it = 0;
            }
        }
        //Выводим остатки буфера
        for (int i = 0; i < it; i++) {
            std::cout << m_Buffer[i] << '\n';
        }
    }
//--------------------------------------------------------------------------------------------------------------------//
    
private:
    //Левая и правая границы z-блока
    size_t m_left, m_right;
    //Шаблонная строка
    const string m_sPattern;
    //Вектор храненящий значений z-функции от шаблонной строки
    std::vector<size_t> m_zFunctionPattern;
    //Длина шаблнной строки
    size_t m_len;
    
    //Размеры буфера
    static const int m_kBufferSize = 100;
    //Буфер нужен чтобы ускорить вывод
    std::array<size_t, m_kBufferSize> m_Buffer;;
    
//--------------------------------------------------------------------------------------------------------------------//
    /*
     * Вычисление значения Z-функции для данного элемента в строке
     * str - строка, в которой ищем вхождение шаблона
     * pos - текущая рассматриваемая позиция в строке, для нее будем искать значени Z-функции
     */
    size_t zFunction(string& str, size_t pos) {
        //Значение Z-функции для данного элемента в строке
        size_t z = std::max(0, std::min(static_cast<int>(m_right - pos),
                                        static_cast<int>(m_zFunctionPattern[pos - m_left])));
        while (pos + z < str.length() && m_sPattern[z] == str[pos + z]) {
            ++z;
        }
        //Обновляем границы Z-блока
        if (z + pos > m_right) {
            m_left = pos;
            m_right = z + pos;
        }
        return z;
    }
};

int main(int argc, const char * argv[])
{
    //Ускоряем ввод (немного помогает)
    std::ios_base::sync_with_stdio(false);
    
    //Строка и шаблон
    string str, pattern;
    std::cin >> pattern >> str;
    
    //Вычислим Z-функцию от шаблона
    CSubstringFinder find(pattern);
    //Решение
    find.findSubstrings(str);
    return 0;
}
