/*
 * Даны два выпуклых многоугольника на плоскости. В первом n точек, во втором m.
 * Определите, пересекаются ли они за O(n + m). Указание. Используйте сумму Минковского.
 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

using std::cin;
using std::cout;
using std::vector;

struct Point{
    Point(const double& _x, const double& _y): x(_x), y(_y){};
    Point(): x(0), y(0){};
    double x;
    double y;

    Point operator+ (const Point& other) const;
};

Point Point::operator+(const Point& other) const {
    return Point(this->x + other.x, this->y + other.y);
}

double const PI = 3.1415926535;
class Vector{
public:
    Vector(const Point& _p1, const Point& _p2): p1(_p1), p2(_p2){
        a.x = _p2.x - _p1.x;
        a.y = _p2.y - _p1.y;
        //Угол между вектоом и Ох.
        if (a.y >= 0) {
            angle = acos(a.x/(sqrt(a.x * a.x + a.y * a.y)));
        }
        else{
            angle = 2 * PI - acos(a.x/(sqrt(a.x * a.x + a.y * a.y)));
        }
    };
    Vector(): p1(0,0), p2(0, 0){
        a.x = 0;
        a.y = 0;
    };

    Point p1;
    Point p2;
    Point a;
    double angle;

    Vector operator+ (const Vector& other) const;
    Vector& operator=(const Vector& right) = default;
};

Vector Vector::operator+(const Vector& other) const {
    return Vector(this->p1 + other.p1, this->p2 + other.p2);
}

bool comp (Vector i, Vector j){
    return (i.angle < j.angle);
}

vector<Vector> sortByAngle(vector<Vector>& polygonEdges, unsigned int n){
    vector<Vector> answer;
    std::merge(polygonEdges.begin(), polygonEdges.begin() + n,
            polygonEdges.begin() + n, polygonEdges.end(),
            std::back_inserter(answer), comp);
    return answer;
}

vector<Vector> minkowskiAddition(vector<Vector>& polygonEdges){
    vector<Vector> result(polygonEdges.size());
    Point begin(0, 0);
    Vector edge(begin, polygonEdges[0].a);
    result[0] = edge;
    for (size_t i = 1; i < polygonEdges.size(); ++i) {
        Point next(polygonEdges[i].a.x, polygonEdges[i].a.y);
        Vector edge(result[i - 1].p2, result[i - 1].p2 + next);
        result[i] = edge;
    }
    return result;
}

bool isOverlap(vector<Vector>& polygonEdges) {
    bool answer = true;
    Point zero(0, 0);
    for (size_t i = 0; i < polygonEdges.size(); ++i) {
        Vector a(polygonEdges[i].p1, polygonEdges[i].p2);
        Vector b(polygonEdges[i].p1, zero);
        if (a.a.x * b.a.y - b.a.x * a.a.y < 0) {
            answer = false;
        }
    }
    return answer;
}

int main(int argc, const char * argv[]) {
    unsigned int n;
    cin >> n;

    vector<Point> polygon1Vertices(n);

    cin >> polygon1Vertices[0].x >> polygon1Vertices[0].y;
    double maxx1 = polygon1Vertices[0].x;
    double maxy1 = polygon1Vertices[0].y;

    for (int i = 1; i < n; ++i) {
        cin >> polygon1Vertices[i].x >> polygon1Vertices[i].y;
        if (maxx1 < polygon1Vertices[i].x) {
            maxx1 = polygon1Vertices[i].x;
        }
        if (maxy1 < polygon1Vertices[i].y) {
            maxy1 = polygon1Vertices[i].y;
        }
    }

    unsigned int m;
    cin >> m;

    vector<Point> polygon2Vertices(m);

    cin >> polygon2Vertices[0].x >> polygon2Vertices[0].y;
    //Преобразуем точку в симетричную относительно (0, 0).
    polygon2Vertices[0].x *= -1;
    polygon2Vertices[0].y *= -1;
    double maxx2 = polygon2Vertices[0].x;
    double maxy2 = polygon2Vertices[0].y;

    for (int i = 1; i < m; ++i) {
        cin >> polygon2Vertices[i].x >> polygon2Vertices[i].y;
        //Преобразуем точку в симетричную относительно (0, 0).
        polygon2Vertices[i].x *= -1;
        polygon2Vertices[i].y *= -1;
        if (maxx2 < polygon2Vertices[i].x) {
            maxx2 = polygon2Vertices[i].x;
        }
        if (maxy2 < polygon2Vertices[i].y) {
            maxy2 = polygon2Vertices[i].y;
        }
    }

    vector<Vector> polygonEdges(n + m);

    //Зададим многоугольники векторами сторон.
    Point p1;
    Point p2;
    for (int i = 0; i < n - 1; ++i) {
        p1 = polygon1Vertices[i];
        p2 = polygon1Vertices[i + 1];
        Vector edge(p1, p2);
        polygonEdges[i] = edge;
    }
    //Замкнем многоугольник.
    {
        Vector edge(polygon1Vertices[n - 1], polygon1Vertices[0]);
        polygonEdges[n - 1] = edge;
    }

    //Зададим многоугольники векторами сторон.
    for (int i = 0; i < m - 1; ++i) {
        p1 = polygon2Vertices[i];
        p2 = polygon2Vertices[i + 1];
        Vector edge(p1, p2);
        polygonEdges[i + n] = edge;
    }
    //Замкнем многоугольник.
    {
        Vector edge(polygon2Vertices[m - 1], polygon2Vertices[0]);
        polygonEdges[n + m - 1] = edge;
    }

    //Сортировка стороны по полярному углу.
    polygonEdges = sortByAngle(polygonEdges, n);

    //Построи сумму Минковского.
    polygonEdges = minkowskiAddition(polygonEdges);

    //Сместим сумму.
    //Идея в том что точки с максимальной x координаторй многоугольников перейдут в точку с максимальной x координатой суммы.
    double maxx = polygonEdges[0].p1.x;
    double maxy = polygonEdges[0].p1.y;
    for (int i = 0; i < n + m; ++i) {
        if (maxx < polygonEdges[i].p1.x) {
            maxx = polygonEdges[i].p1.x;
        }
        if (maxy < polygonEdges[i].p1.y) {
            maxy = polygonEdges[i].p1.y;
        }
    }

    for (int i = 0; i < n + m; ++i) {
        polygonEdges[i].p1.x -= maxx - (maxx1 + maxx2);
        polygonEdges[i].p1.y -= maxy - (maxy1 + maxy2);
        polygonEdges[i].p2.x -= maxx - (maxx1 + maxx2);
        polygonEdges[i].p2.y -= maxy - (maxy1 + maxy2);
    }

    //Многоугольники пересекаются если (0, 0) лежит в пределах суммы Минковского.
    if (isOverlap(polygonEdges)){
        cout << "YES";
    }
    else{
        cout << "NO";
    }
    return 0;
}
