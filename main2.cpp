/*
 * Реализовать набор преобразований строки в префикс-функцию, z-функцию и обратно, а также из префикс-функции в
 * z-функциюи обратно (6 шт). Все преобразования должны работать за линейное время (размер алфавита считаем константой).
 * Преобразование в строку должно выдавать лексикографически минимально возможную строку.
 */

#include <iostream>
#include <string>
#include <vector>

using std::string;

//--------------------------------------------------------------------------------------------------------------------//
//Префикс-функция в строку и обратно.

/*
 * Функция для построения префикс-функции от данной строки.
 * str - строка, для которой нужно построить префикс-функцию.
 */
std::vector<int> prefixFunction(const string& str) {
    //Вектор, содержащий значения префикс-функции от строки.
    std::vector<int> prefix(str.size(), 0);
    
    int prevPrefix;
    for (int i = 1; i < str.size(); ++i) {
        //Удобно ввести переменную, равную значению префикс-функции от предыдущего элемента.
        prevPrefix = prefix[i - 1];
        //Подберем бордер наибольшей длины.
        while (prevPrefix > 0 && str[i] != str[prevPrefix]) {
            prevPrefix = prefix[prevPrefix - 1];
        }
        if (str[i] == str[prevPrefix]) {
            prevPrefix++;
        }
        prefix[i] = prevPrefix;
    }
    return prefix;
}

/*
 * Функция для построения лексикографически минимальной строки по префикс-функции.
 * prefix - вектор со значениями префикс-функции.
 */
std::string prefixToString(const std::vector<int>& prefix) {
    //Вектор, хранящий номер в алфавите буквы, которая должна стоять на этой позиции.
    std::vector<int> charNumber(prefix.size(), 0);
    //Размер текущего используемого алфавита.
    int alphabet_size = 1;
    
    //Размер латинского алфавита.
    const int latinAlphabetSize = 26;
    
    for(size_t i = 1; i < prefix.size(); ++i) {
        if (prefix[i] == 0){
            //Вектор с использованными буквами латинского алфавита.
            std::vector<bool> usedChars(latinAlphabetSize, false);
            //Удобно ввести переменную, равную значению префикс-функции от предыдущего элемента.
            int prevPrefix = prefix[i - 1];
            
            while (prevPrefix > 0) {
                usedChars[charNumber[prevPrefix]] = true;
                prevPrefix = prefix[prevPrefix - 1];
            }
            
            charNumber[i]= -1;
            for (int j = 1; j < alphabet_size; ++j) {
                if (!usedChars[j]) {
                    charNumber[i] = j;
                    break;
                }
            }
            //Если нельзя вставить какую-то использованную букву, то вставляем новую и увеличиваем алфвавит.
            if (charNumber[i] == -1) {
                charNumber[i] = alphabet_size++;
            }
        }
        else {
            charNumber[i] = charNumber[prefix[i] - 1];
        }
    }
    
    //Строка, являющаяся ответом в задаче.
    string result = "";
    for (int i = 0; i < charNumber.size(); ++i) {
        result += static_cast<char>(static_cast<int>('a') + charNumber[i]);
    }
    return result;
}
//--------------------------------------------------------------------------------------------------------------------//
//Z-функция от строки. Получить строку из z-функции: строим префикс по z-функции, затем строку п опрефикс-функции.

/*
 * Функция для построения z-функции от данной строки.
 * str - строка, для которой нужно построить префикс-функцию.
 */
std::vector<int> zFunction(const string& str) {
    //Границы Z-блока.
    size_t left = 0;
    size_t right = 0;
    
    const size_t len = str.size();
    //Вектор, хранящий значения Z-функции от данной строки.
    std::vector<int> zf(len);
    
    for (size_t i = 1; i < len; ++i) {
        zf[i] = std::max(0, std::min(static_cast<int>(right - i), zf[i - left]));
        while (i + zf[i] < len && str[zf[i]] == str[i + zf[i]]) {
            zf[i]++;
        }
        //Новые границы Z-блока.
        if (i + zf[i] > right) {
            left = i;
            right = i + zf[i];
        }
    }
    return zf;
}

//--------------------------------------------------------------------------------------------------------------------//
//Z-функция некоторой строки в её префикс-функцию и обратно.

/*
 * Функция для построения префикс-функции некоторой строки по её Z-функции.
 * zf - вектор со значениями Z-функции некоторой строки.
 */
std::vector<int> zFunctionToPrefixFunction(const std::vector<int>& zf) {
    //Вектор, содержащий значения префикс-функции некоторой строки.
    std::vector<int> prefix(zf.size(), 0);
    for (int i = 1; i < zf.size(); ++i) {
        for (int j = zf[i] - 1; j > 0; --j) {
            if (prefix[i + j] > 0) {
                break;
            }
            else{
                prefix[i + j] = j + 1;
            }
        }
    }
    return prefix;
}

/*
 * Функция для построения Z-функции некоторой строки по её префикс-функции.
 * prefix - вектор со значениями префикс-функции некоторой строки.
 */
std::vector<int> prefixFunctionTozFunction(const std::vector<int>& prefix) {
    //Вектор, содержащий значения Z-функции некоторой строки.
    std::vector<int> zf(prefix.size(), 0);
    for (int i = 0; i < prefix.size(); ++i) {
        if (prefix[i] > 0) {
            zf[i - prefix[i] + 1] = prefix[i];
        }
    }
    zf[0] = static_cast<int>(prefix.size());
    int i = 0;
    while (i < prefix.size()) {
        int t = i;
        if (zf[i] > 0) {
            for (int j = 0; j < zf[i] - 1; ++j) {
                if (zf[i + j] > zf[j]) {
                    break;
                }
                zf[i + j] = std::min(zf[j], zf[i] - j);
                t = i + j;
            }
        }
        i = t + 1;
    }
    return zf;
}


int main(int argc, const char * argv[]) {
    string s;
    std::vector<int> m;
    std::cin >> s;
    m = prefixFunction(s);
    for (int i = 0; i < s.size(); ++i) {
        std::cout << m[i];
    }
    return 0;
}
