/*
 * Даны два отрезка в пространстве (x1, y1, z1) - (x2, y2, z2) и (x3, y3, z3) - (x4, y4, z4).
 * Найдите расстояние между отрезками.
 */
#include <cmath>
#include <iostream>
#include <iomanip>

using std::cin;
using std::cout;

struct Point{
    Point(const double& _x, const double& _y, const double& _z): x(_x), y(_y), z(_z){};
    double x;
    double y;
    double z;
};

struct Vector{
    Vector(const double& _x, const double& _y, const double& _z): x(_x), y(_y), z(_z){};
    Vector(const Point& p0, const Point& p1): x(p1.x - p0.x), y(p1.y - p0.y), z(p1.z - p0.z){};
    Vector(const Vector& v): x(v.x), y(v.y), z(v.z){};

    double x;
    double y;
    double z;

    double Norm(){
        return sqrt(x * x + y * y + z * z);
    }

    Vector operator* (double i) const;
    Vector operator+ (const Vector& other) const;
    Vector operator- (const Vector& other) const;
};


Vector Vector::operator* (double i) const {
    return Vector(this->x * i, this->y * i, this->z * i);
}

Vector Vector::operator+ (const Vector& other) const {
    return Vector(this->x + other.x, this->y + other.y, this->z + other.z);
}

Vector Vector::operator- (const Vector& other) const {
    return Vector(this->x - other.x, this->y - other.y, this->z - other.z);
}

double scalarDot(const Vector &v1, const Vector &v2){
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

struct Segment{
    Segment(const Point p0, const Point p1): P0(p0.x, p0.y, p0.z), P1(p1.x, p1.y, p1.z), a(p0, p1){};
    Vector a;
    Point P0;
    Point P1;
};

double p(double c){     //Функция, не дающая в ответе выйти за пределы отрезка.
    if (c > 1) {
        return 1;
    }
    if (c < 0) {
        return 0;
    }
    return c;
}

const double EPSILON = 1e-8;    //Константа, задающая точность работы с double.

double distanceBetweenTwoSegment(const Segment &S1, const Segment &S2) {
    Vector u(S1.P1, S1.P0);     //Направляющий вектор первого отрезка.
    Vector v(S2.P1, S2.P0);     //Направляющий вектор второго отрезка.
    Vector w0(S1.P0, S2.P0);    //Вектор, соединяющий начальные точки отрезков.

    double uu = scalarDot(u, u);
    double uv = scalarDot(u, v);
    double vv = scalarDot(v, v);
    double uw0 = scalarDot(u, w0);
    double vw0 = scalarDot(v, w0);

    //Формула sC = (uv * vw0 - vv * uw0)/uu * vv - uv * uv. Выделим знаменатель отдельно в переменную D.
    double D = uu * vv - uv * uv;   //Всегда >= 0.

    //Отрезки представим как u*s, v*t. Параметры s,t из отрезка [0, 1].
    double sC = 0, tC = 0;      //Значения sC, tC -- значения параметров s и t,
                                // при которых достигается наименьшее растояние между отрезками.
    //Чтобы меньше работать с результатом деления, будем работать с числителем и знаменателем по отдельности.
    double sN, tN;
    double sD, tD;
    sD = D;
    tD = D;

    Vector distance = w0;

    if (D < EPSILON) {              //Случай когда отрезки почти параллельны или один/оба отрезка -- точки.
        if (vv == 0 && uu != 0) {   //Вместо второго отрезка дана точка.
            if (uw0 >= 0) {         //Если точка позади отрезка.
                sC = 0;             //У отрезка подходит начальная точка.
            }
            else
            if (uw0 <= -uu) {       //Если точка впереди отрезка.
                sC = 1;             //У отркзка подходит конец.
            }
            else {                  //Если точка где-то посередине
                sC = -uw0 / uu;     //Ищем наименьшее растсояние через производную.
            }
        }
        else
        if (uu == 0 && vv != 0) {   //Вместо первого отрезка дана точка. Аналогично предыдущему случаю
            if (vw0 >= 0) {
                tC = 0;
            }
            else
            if (vw0 <= -vv) {
                tC = 1;
            }
            else {
                tC = vw0 / vv;
            }
        }
        else
        if (uu == 0 && vv == 0) {   //Даны две точки.
            return w0.Norm();       //Просто расстояние между точками.
        }
        else {                      //Даны два параллельных отрезка.
            if (uw0 >= 0) {         //Второй отрезок позади первого
                sC = 0;             //У первого отрезка подходит начало
                tC = vw0 / vv;      //У второго отрезка подходящую точку ищем через производную.
            }
            else{                   //Случай когда второй отрезок впереди первого
                sC = 1;             //У первого орезка подходит конец.
                tC = (vw0 + uv) / vv;   //У второго отрезка подходящую точку ищем через производную.
            }
        }
        distance = w0 + (u * p(sC) - v * p(tC));    //Вектор, соединяющий ближайшие друг к другу точки.
        return distance.Norm();
    }

    //Формула sC = (uv * vw0 - vv * uw0)/uu * vv - uv * uv.
    sN = uv * vw0 - vv * uw0;   //Числитель для первого параметра.
    tN = uu * vw0 - uv * uw0;   //Числитель для второго параметра.

    //Так как знаменатель всегда >=0, то на знак дроби влияет только числитель.
    if (sN < 0) {       //Если искомая точка певрого отрезка расположена на прямой где-то до отрезка.
        sN = 0;         //У певого отрезка подходит начало.
        tN = vw0;       //Подходящую точку второго отрезка вычислем через производную при условии что sC = 0. Числитель.
        tD = vv;        //Знаменатель.
    }
    else
    if (sN >= sD) {     //Если искомая точка певрого отрезка расположена на прямой где-то после отрезка.
        sN = sD;        //У первого отрезка подходит конец.
        tN = vw0 + uv;  //Подходящую точку второго отрезка вычислем через производную при условии что sC = 0. Числитель.
        tD = vv;        //Знаменатель.
    }

    //Аналогично случаю с s рассматриваем для t.
    if (tN < 0) {           //Если искомая точка вторго отрезка расположена на прямой где-то до отрезка.
        tN = 0;             //У второго отрезка подходит начало.
        if (uw0 > 0) {
            sN = sD;
        }
        else
        if (uu + uw0 <= 0) {
            sN = sD;
        }
        else {
            sN = -uw0;
            sD = uu;
        }
    }
    else if (tN >= tD) {    //Если искомая точка вторго отрезка расположена на прямой где-то после отрезка.
        tN = tD;            //У второго отрезка подходит конец.
        if (uv - uw0 <= 0) {
            sN = 0;
        }
        else
        if(uv - uw0 >= uu){
            sN = sD;
        }
        else {
            sN = uv - uw0;
            sD = uu;
        }
    }

    sC = sN / sD;           //По формуле учитывая рассмотренные случаи вычисляем значение sC.
    tC = tN / tD;           //По формуле учитывая рассмотренные случаи вычисляем значение tC.
    distance = w0 + u * p(sC) - v * p(tC);          //Вектор, соединяющий ближайшие точки.

    if (distance.x != 0 || distance.y != 0|| distance.z != 0) {   //Если ненулевой вектор.
        return distance.Norm();
    }
    else return 0;
}

int main()
{
    double x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
    cin >> x1 >> y1 >> z1 >> x2 >> y2 >> z2 >> x3 >> y3 >> z3 >> x4 >> y4 >> z4;
    Point p1(x1, y1, z1), p2(x2, y2, z2), p3(x3, y3, z3), p4(x4, y4, z4);
    Segment S1(p1, p2);
    Segment S2(p3, p4);

    cout << std::fixed << std::setprecision(8) << distanceBetweenTwoSegment(S1, S2);
    return 0;
}
