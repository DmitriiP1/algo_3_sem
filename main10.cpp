#include <iostream>
#include <vector>
#include <string>

using std::string;
using std::vector;
using std::cout;
using std::cin;
using std::endl;
using std::istream;
using std::ostream;

class BigInteger{
public:
    BigInteger(): data(vector<int> (1, 0)) {}//+
    BigInteger(long long a);//+
    
    BigInteger(string  const &str): data(vector<int> (0,0)) {
        for (unsigned int i = (str[0] == '-'); i < str.length(); ++i){
            (*this)[i - ((str[0] == '-') ? 1 : 0)] = (str[i] - '0') * ((str[0] == '-')? -1: 1);
        }
    }
    
    BigInteger& operator=(BigInteger const& other);
    
    BigInteger& operator+=(BigInteger const& other);
    BigInteger& operator-=(BigInteger const& other);
    BigInteger& operator*=(BigInteger const& other);
    BigInteger& operator*=(int other);
    BigInteger& operator/=(BigInteger other);
    BigInteger& operator%=(BigInteger other);

    BigInteger operator-() const;
    
    BigInteger& operator++();
    const BigInteger operator++(int);
    BigInteger& operator--();
    const BigInteger operator--(int);
    
    explicit operator bool() const;
    bool operator==(BigInteger const& other) const;
    bool operator!=(BigInteger const& other) const;
    bool operator<(BigInteger const& other) const;
    bool operator>(BigInteger const& other) const;
    bool operator<=(BigInteger const& other) const;
    bool operator>=(BigInteger const& other) const;
    
    string toString() const;
    
    friend BigInteger operator+(BigInteger a, BigInteger const& b);
    friend BigInteger operator-(BigInteger a, BigInteger const& b);
    friend BigInteger operator*(BigInteger a, BigInteger const& b);
    friend BigInteger operator/(BigInteger a, BigInteger const& b);
    friend BigInteger operator%(BigInteger a, BigInteger const& b);
    
    int length() const {
        return (int)data.size();
    }
    
    int digit(int index) const {
        return index < 0 ? 0 : data[index];
    }
    
    int getSign() const {
        return digit(0) == 0 ? 0 : (digit(0) < 0 ? -1 : 1);
    }

private:
    vector<int> data;
    
    static void swap(int& first, int& second);
    void moveRight(int deltaPositions);
    
    static void reverseVector(vector<int>& argumentVector);
    void normDigit();
    void deleteLeaderZerosForReversed();
    BigInteger& Normalize();
    
    bool compareSubvectorWithIntervalForDivision(int lBound, int rBound, BigInteger const& divider) const;
    void substructOnSubvectorForDivision(int lBound, int rBound, BigInteger const& divider);
    
    BigInteger& changeSign();
    
    int& operator[](unsigned int index);
    
    bool isNegative() const {
        return getSign() == -1;
    }
    
    bool isPositive() const {
        return getSign() == 1;
    }
    
    bool isZero() const {
        return getSign() == 0;
    }
    
    BigInteger& NearestBigInteger(bool isNext);
    
    static int abs(int num);
    static int max(int first, int second);
    static int min(int first, int second);
};

BigInteger::BigInteger(long long a) {
    if (a == 0) {
        data.assign(1, 0);
    }
    else {
        if (a > 0) {
            while (a) {
                data.push_back(a % 10);
                a /= 10;
            }
        }
        else {
            a *= -1;
            while (a) {
                data.push_back(-(a % 10));
                a /= 10;
            }
        }
    }
    reverseVector(data);
}

string BigInteger::toString() const {
    string resultString = "";
    if (data[0] < 0) {
        resultString += "-";
    }
    for (int i = 0; i < length(); ++i) {
        resultString += '0' + abs(data[i]);
    }
    return resultString;
}

BigInteger BigInteger::operator-() const {
    BigInteger temp = *this;
    for (int i = 0; i < length(); ++i) {
        temp.data[i] *= -1;
    }
    return temp;
}

BigInteger& BigInteger::operator+=(BigInteger const& other) {
    moveRight(other.length() - length());
    
    for (int i = 1; i <= min(length(), other.length()); ++i) {
        (*this)[max(length(), other.length()) - i] = digit(length() - i) + other.digit(other.length() - i);
    }
    
    for (int i = 0; i < other.length() - length(); ++i) {
        (*this)[other.length() - length() - i - 1] = other.digit(other.length() - length() - i - 1);
    }
    
    Normalize();
    return *this;
}

BigInteger& BigInteger::operator-=(BigInteger const& other) {
    moveRight(other.length() - length());
    
    for (int i = 1; i <= min(length(), other.length()); ++i) {
        (*this)[max(length(), other.length()) - i] = digit(length() - i) - other.digit(other.length() - i);
    }
    
    Normalize();
    return *this;
}

BigInteger& BigInteger::operator*=(BigInteger const& other) {
    moveRight(other.length());
    for (int i = 0; i < length(); ++i) {
        int tempDigit = 0;
        for (int j = 0; j < other.length() && i + j < length(); ++j) {
            tempDigit += (digit(i + j)) * other.digit(other.length() - j - 1);
        }
        (*this)[i] = tempDigit;
    }
    Normalize();
    return *this;
}

BigInteger& BigInteger::operator*=(int other) {
    if (other == 10) {
        if (*this != 0) {
            data.push_back(0);
        }
        return *this;
    }
    
    if (other == -1) {
        changeSign();
        return *this;
    }
    
    for (int i = 0; i < length(); ++i) {
        (*this)[i] *= other;
    }
    
    Normalize();
    return *this;
}

BigInteger& BigInteger::operator/=(BigInteger other) {
    bool sign = (getSign() == other.getSign());
    if (isNegative()) {
        changeSign();
    }
    
    BigInteger diider = other;
    if (other.isNegative()) {
        diider.changeSign();
    }
    
    if ((*this) < diider) {
        return *this = BigInteger(0);
    }
    
    BigInteger quotient(0);
    int lBound = 0, rBound = diider.length();
    while (rBound <= length()) {
        int i = 1;
        while (compareSubvectorWithIntervalForDivision(lBound, rBound, diider * i)) {
            ++i;
        }
        --i;
        substructOnSubvectorForDivision(lBound, rBound, diider * i);
        quotient[rBound] = i;
        while (lBound < rBound && digit(lBound) == 0) {
            ++lBound;
        }
        ++rBound;
    }
    return (*this = (sign ? quotient : quotient.changeSign())).Normalize();
}

BigInteger& BigInteger::operator%=(BigInteger other) {
    bool sign = getSign() == other.getSign();
    if (isNegative()){
        changeSign();
    }
    if (other.isNegative()){
        other.changeSign();
    }
    if ((*this) < other){
        return sign ? *this : (*this).changeSign();
    }
    BigInteger quotient(0);
    int lBound = 0;
    int rBound = other.length();
    while (rBound <= length()) {
        int i = 0;
        for (i = 1; compareSubvectorWithIntervalForDivision(lBound, rBound, other * i); ++i) ;
        --i;
        substructOnSubvectorForDivision(lBound, rBound, other * i);
        quotient[rBound] = i;
        while (lBound < rBound && digit(lBound) == 0)
            ++lBound;
        ++rBound;
    }
    Normalize();
    if (!sign) {
        changeSign();
    }
    return *this;
}

BigInteger& BigInteger::operator++() {
    return NearestBigInteger(true);
}

const BigInteger BigInteger::operator++(int) {
    BigInteger temp = *this;
    ++(*this);
    return temp;
}

BigInteger& BigInteger::operator--() {
    return NearestBigInteger(false);
}

const BigInteger BigInteger::operator--(int) {
    BigInteger tempBigInteger = *this;
    --(*this);
    return tempBigInteger;
}

BigInteger::operator bool() const {
    return digit(0) != 0;
}

bool BigInteger::operator==(BigInteger const& other) const {
    if (getSign() != other.getSign())
        return false;
    return (*this - other).isZero();
}

bool BigInteger::operator<(BigInteger const& other) const {
    if (getSign() < other.getSign()){
        return true;
    }
    if (getSign() > other.getSign()){
        return false;
    }
    if (length() != other.length()) {
        return getSign() == 1 ? length() < other.length() : length() > other.length();
    }
    return (*this - other).isNegative();
}

bool BigInteger::operator<=(BigInteger const &other) const {
    return !(*this > other);
}

bool BigInteger::operator>(BigInteger const &other) const {
    return other < *this;
}

bool BigInteger::operator>=(BigInteger const &other) const {
    return !(*this < other);
}

bool BigInteger::operator!=(BigInteger const &other) const {
    return !(*this == other);
}

BigInteger& BigInteger::operator=(BigInteger const &other) {
    if (this == &other)
        return *this;
    data = other.data;
    return *this;
}

//--------------------------------------------------------------------------------------------------------------------//
int BigInteger::abs(int num) {
    return num >= 0 ? num : -num;
}

int BigInteger::max(int first, int second) {
    return first > second ? first : second;
}

int BigInteger::min(int first, int second) {
    return first > second ? second : first;
}

void BigInteger::swap(int &first, int &second) {
    int tmp;
    tmp = first;
    first = second;
    second = tmp;
}

void BigInteger::reverseVector(vector<int> &v) {
    for (unsigned int i = 0; i < v.size() / 2; ++i) {
        swap(v[i], v[v.size() - i - 1]);
    }
}

void BigInteger::normDigit() {
    deleteLeaderZerosForReversed();
    if (data.back() < 0) {
        for (unsigned int i = 0; i < data.size() - 1; ++i) {
            if ((*this)[i] > 0) {
                (*this)[i + 1] += (*this)[i] / 10 + ((*this)[i] % 10 > 0);
                (*this)[i] -= ((*this)[i] / 10 + ((*this)[i] % 10 > 0)) * 10;
            } else {
                (*this)[i + 1] += (*this)[i] / 10;
                (*this)[i] %= 10;
            }
        }
    }
    else {
        for (unsigned int i = 0; i < data.size() - 1; ++i) {
            if ((*this)[i] > 0) {
                (*this)[i + 1] += (*this)[i] / 10;
                (*this)[i] %= 10;
            } else {
                (*this)[i + 1] -= -(*this)[i] / 10 + ((*this)[i] % 10 < 0);
                (*this)[i] += (-(*this)[i] / 10 + ((*this)[i] % 10 < 0)) * 10;
            }
        }
    }
    while (data.back() < -9 || data.back() > 9) {
        data.push_back(data.back() / 10);
        (*this)[data.size()-2] %= 10;
    }
    deleteLeaderZerosForReversed();
}

void BigInteger::deleteLeaderZerosForReversed() {
    while (data.size() > 1 && data.back() == 0) {
        data.pop_back();
    }
}

BigInteger& BigInteger::Normalize() {
    reverseVector(data);
    normDigit();
    reverseVector(data);
    return *this;
}

void BigInteger::moveRight(int deltaPositions) {
    if (deltaPositions > 0) {
        for (int i = deltaPositions + length() - 1; i >= deltaPositions; --i) {
            (*this)[i] = (*this)[i - deltaPositions];
        }
        
        for (int i = 0; i < deltaPositions; ++i) {
            (*this)[i] = 0;
        }
    }
}

bool BigInteger::compareSubvectorWithIntervalForDivision(int lBound, int rBound, BigInteger const& divider) const {
    if (divider.length() > rBound - lBound) {
        return false;
    }
    if (divider.length() < rBound - lBound) {
        return true;
    }
    for (int i = lBound; i < rBound; ++i) {
        if (digit(i) < divider.digit(i - lBound)){
            return false;
        }
        else{
            if (digit(i) > divider.digit(i - lBound)){
                return true;
            }
        }
    }
    return true;
}

void BigInteger::substructOnSubvectorForDivision(int lBound, int rBound, BigInteger const& divider) {
    int delta = rBound - lBound - divider.length();
    for (int i = rBound - 1; i >= lBound; --i) {
        if (digit(i) < divider.digit(i - lBound - delta)) {
            --(*this)[i - 1];
            (*this)[i] -= -10 + divider.digit(i - lBound - delta);
        } else
            (*this)[i] -= divider.digit(i - lBound - delta);
    }
}

BigInteger& BigInteger::changeSign() {
    for (int i = 0; i < length(); ++i) {
        (*this)[i] *= -1;
    }
    return *this;
}

int& BigInteger::operator[](unsigned int index) {
    while (index >= data.size()) {
        data.push_back(0);
    }
    return data[index];
}

BigInteger& BigInteger::NearestBigInteger(bool isNext) {
    if (isZero()) {
        data[0] = isNext? 1 : -1;
        return *this;
    }
    if (length() == 1 && data[0] == (isNext ? -1 : 1)) {
        data[0] = 0;
        return *this;
    }
    int iter = length() - 1;
    while (iter >= 0 && data[iter] == 9 * (isNext ? (isPositive() ? 1 : 0) : (isPositive() ? 0 : -1))) {
        --iter;
    }
    if (isPositive()) {
        if (iter < 0) {
            data[0] = isNext ? 1 : -1;
            for (unsigned i = 1; i < data.size(); ++i) {
                data[i] = 0;
            }
            data.push_back(0);
        }
        else {
            isNext ? ++data[iter] : --data[iter];
            while (++iter < length()) {
                data[iter] = 0;
            }
        }
    }
    else {
        if (iter == 0) {
            if (data[0] == (isNext ? -1 : 1)) {
                data.pop_back();
                data.assign(data.size(), (isNext ? -9 : 9));
            }
            else {
                isNext ? ++data[0] : --data[0];
                while (++iter < length()) {
                    data[iter] = (isNext ? -9 : 9);
                }
            }
        }
        else {
            isNext ? ++data[iter] : --data[iter];
            while (++iter < length()) {
                data[iter] = 0;
            }
        }
    }
    return *this;
}


BigInteger operator+(BigInteger a, BigInteger const& b) {
    return a += b;
}

BigInteger operator-(BigInteger a, BigInteger const& b) {
    return a -= b;
}

BigInteger operator*(BigInteger a, BigInteger const& b) {
    return a *= b;
}

BigInteger operator/(BigInteger a, BigInteger const& b) {
    return a /= b;
}

BigInteger operator%(BigInteger a, BigInteger const& b) {
    return a %= b;
}

istream& operator>>(istream& cin, BigInteger& input) {
    string tempScanningString;
    cin >> tempScanningString;
    input = BigInteger(tempScanningString);
    return cin;
}

ostream& operator<<(ostream& cout, BigInteger const& output) {
    cout << output.toString();
    return cout;
}
